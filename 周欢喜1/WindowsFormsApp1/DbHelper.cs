﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DbHelper
    {
        private static readonly string connStr = "server=.;database=Schools;uid=sa;pwd=20120820zhx";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(connStr);
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sql, sqlConnection);
            sqlConnection.Open();
            DataTable dt = new DataTable();
            sqlDataAdapter.Fill(dt);
            return dt;
        }
        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(connStr);
            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
            sqlConnection.Open();
            var res = sqlCommand.ExecuteNonQuery();
            return res;

        }
    }
}
